# manolete.bitbucket.org

Source for the page at [manolete.bitbucket.org](http://manolete.bitbucket.org).

The goal was to have a minimal css for the blog layout and a D3js worldmap with places of personal interest.

The python scripts (update.py) is responsible for:

* creating the blog entries based on the *.txt entries 

* using the _*.html templates to output the html files for the blog 

* minifying the css / html