var renderWorld = function() {
  var width = $(window).width()  / 2;
  var height = 800;
  var scale = width / 1000 * 100;

  var projection = d3.geo.mercator()
    .center([0, 0])
    .scale(scale)
    .rotate([0, 0])
    .translate([width / 3, height / 1.5]);

  var tooltip = d3.select("body")
    .append("div")
    .style("position", "relative")
    .style("z-index", "10")
    .style("visibility", "hidden");

  var svg = d3.select("#map").append("svg")
    .attr("width", width)
    .attr("height", height)
    .on("click", function(d) {
      tooltip.style("visibility", "hidden");
    });

  var path = d3.geo.path()
    .projection(projection);

  var g = svg.append("g");

  var updatePoints = function(year) {
    d3.csv("places/img/places" + year + ".csv", function(error, data) {
      g.selectAll("circle").remove();
      g.selectAll("circle")
        .data(data)
        .enter()
        .append("circle")
        .attr("cx", function(d) {
          return projection([d.lon, d.lat])[0];
        })
        .attr("cy", function(d) {
          return projection([d.lon, d.lat])[1];
        })
        .attr("r", 5)
        .style("fill", "green")
        .on('click', function(data) {
          var x = d3.event.pageX;
          var y = d3.event.pageY;
          $('#pictureContainer').fadeOut(function() {
            $("#spinner").css({
              top: y,
              left: x
            }).fadeIn();
            $('#info').html('<h2 class="lobster">' + data.place + ' ' + data.date + '</h2><p class="center">' + data.text + '</p>').fadeIn();
            var url = "places/img/" + year + '/' + data.url;
            var image = new Image();
            image.src = url;
            image.style = "max-width: 100%; max-height: 100%;"
            image.addEventListener('click', function() { 
              window.open(this.src)
             }, false);
            image.addEventListener('load', function() { 
                  $('#picture').delay(400).html(image);
                  $('#pictureContainer').fadeIn();
                  $("#spinner").fadeOut();
             }, false);
          });
        })
    });
  };

  // load and display the World
  d3.json("places/js/world.json", function(error, topology) {
    g.selectAll("path")
      .data(topojson.object(topology, topology.objects.countries)
        .geometries)
      .enter()
      .append("path")
      .attr("d", path);
      updatePoints(currentYear);
  });

  // zoom and pan
  var zoom = d3.behavior.zoom()
    .on("zoom", function() {
      g.attr("transform", "translate(" +
        d3.event.translate.join(",") + ")scale(" + d3.event.scale + ")");
      g.selectAll("circle")
        .attr("d", path.projection(projection))
        .attr("r", 5 / d3.event.scale)
      g.selectAll("path")
        .attr("d", path.projection(projection));
    });

  svg.call(zoom);

  var currentYear = new Date().getFullYear();
  $(".year").click(function(e) {
    e.preventDefault();
    var element = $(e.target);
    updatePoints(element.text());
  });
};

document.addEventListener('turbolinks:load', renderWorld);