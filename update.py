import glob, os, datetime, sys, re
import subprocess

# minimize css files with less
subprocess.check_call(['lessc', '--clean-css', './css/styles.less', './css/styles.min.css'], cwd=os.path.dirname(os.path.realpath(__file__)))

#header
headerTemplate = open(os.path.join(sys.path[0], "_header.html"), "r").read()

# functions
def minimize(htmlString):
    template = re.sub(">\s*<","><", htmlString.strip().replace('\n',' ').replace('\t',' ').replace('\r',' '))
    return re.sub("(<!--.*?-->)", "", template)

def replaceCss(htmlString, prefix):
    template = htmlString.replace('<link rel="stylesheet" href="' + prefix + 'css/styles.css">', '<link rel="stylesheet" href="' + prefix + 'css/styles.min.css">')
    template = template.format(header = headerTemplate)
    return template

# main page
template = open(os.path.join(sys.path[0], "_index.html"), "r").read()
template = replaceCss(template, '')
indexHtml = open(os.path.join(sys.path[0], 'index.html'), 'w+')
minimizedIndex = minimize(template)
indexHtml.write(minimizedIndex)
indexHtml.close()

#journey
template = open(os.path.join(sys.path[0], "journey/_index.html"), "r").read()
template = replaceCss(template, '')
indexHtml = open(os.path.join(sys.path[0], 'journey/index.html'), 'w+')
minimizedIndex = minimize(template)
indexHtml.write(minimizedIndex)
indexHtml.close()

#map
template = open(os.path.join(sys.path[0], "places/_index.html"), "r").read()
template = replaceCss(template, '')
indexHtml = open(os.path.join(sys.path[0], 'places/index.html'), 'w+')
minimizedIndex = minimize(template)
indexHtml.write(minimizedIndex)
indexHtml.close()

#me
template = open(os.path.join(sys.path[0], "me/_index.html"), "r").read()
template = replaceCss(template, '')
indexHtml = open(os.path.join(sys.path[0], 'me/index.html'), 'w+')
minimizedIndex = minimize(template)
indexHtml.write(minimizedIndex)
indexHtml.close()

#blog
os.chdir("blog/entries")

entries = [(x, datetime.datetime.strptime(x[0:8], "%d%m%Y")) for x in glob.glob("*.txt")]
entries.sort(key=lambda x: x[1], reverse=True)

baseHtml =  open(os.path.join(sys.path[0],'blog/_index.html')).read()
entryLinks = ''.join([ '<li><a href="/blog/{filename}.html">{name}</a></li>'
    .format(name = x[1].strftime("%d-%m-%Y"), filename = 'index' if entries[0] == x else x[0].replace('.txt', '')) for x in entries])

for entry in entries:
    entryLines = open(os.path.join(sys.path[0], "blog/entries/" + entry[0])).read().splitlines()    
    indexHtml = baseHtml.format(header = headerTemplate, title = entryLines[0], text = entryLines[1], entries = entryLinks, date=entry[1].strftime("%d-%m-%Y"), pictureName = entry[0].replace(".txt", ''), year = entry[1].strftime("%Y"))
    filename = 'index.html' if entry == entries[0] else entry[0].replace('.txt', '') + '.html'
    indexHtmlFile = open(os.path.join(sys.path[0], 'blog/' + filename), 'w+')
    indexHtml = replaceCss(indexHtml, '../')
    minimized = minimize(indexHtml)
    indexHtmlFile.write(minimized)
    indexHtmlFile.close()
