import glob, os, datetime, sys

os.chdir("entries")

entries = [(x, datetime.datetime.strptime(x[0:8], "%d%m%Y")) for x in glob.glob("*.txt")]
entries.sort(key=lambda x: x[1], reverse=True)

baseHtml =  open(os.path.join(sys.path[0],'base.html')).read()
firstEntry = open(os.path.join(sys.path[0], "entries/" + entries[0][0])).read().splitlines()
entryLinks = ''.join([ '<div><a id="{filename}" class="entry">{name}</a></div>'.format(name = x[1].strftime("%d-%m-%Y"), filename = x[0]) for x in entries])
indexHtml = baseHtml.format(title = firstEntry[0], text = firstEntry[1], entries = entryLinks, pictureName = entries[0][0].replace(".txt", ''))
indexHtmlFile = open(os.path.join(sys.path[0], 'index.html'), 'r+')
indexHtmlFile.write(indexHtml)
indexHtmlFile.close()
