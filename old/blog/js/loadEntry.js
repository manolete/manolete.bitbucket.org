$(document).ready(function() {
  $(".entry").on("click", function(e) {
    var fileName = $(e.target).attr('id');
    var fileNameWOEnding = fileName.replace(".txt", "");
    var year = fileName.substring(4,8);
    var pictureString = '<a href="../map/img/' + year +'/' + fileNameWOEnding + '.JPG" target="_blank"><img src="../map/img/'  + year +'/' + fileNameWOEnding +'.JPG"></a>';
    $('#picture').html(pictureString);

    $.get("entries/" + fileName, function(content) {
      var splitted = content.split(/\n/);
      $('#title').text(splitted[0]);
      $('#text').text(splitted[1]);
    });
  });
});
