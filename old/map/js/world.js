$(document).ready(function() {
  var width = $(document).width();
  var height = $(document).height();
  var scale = width / 1000 * 100;

  var projection = d3.geo.mercator()
    .center([0, 0])
    .scale(scale)
    .rotate([0, 0])
    .translate([width / 3, height / 1.5]);

  var tooltip = d3.select("body")
    .append("div")
    .style("position", "absolute")
    .style("z-index", "10")
    .style("visibility", "hidden");

  var svg = d3.select("#map").append("svg")
    .attr("width", width)
    .attr("height", height)
    .on("click", function(d) {
      tooltip.style("visibility", "hidden");
    });

  var path = d3.geo.path()
    .projection(projection);

  var g = svg.append("g");

  var updatePoints = function(year) {
    d3.csv("img/places" + year + ".csv", function(error, data) {
      g.selectAll("circle").remove();
      g.selectAll("circle")
        .data(data)
        .enter()
        .append("circle")
        .attr("cx", function(d) {
          return projection([d.lon, d.lat])[0];
        })
        .attr("cy", function(d) {
          return projection([d.lon, d.lat])[1];
        })
        .attr("r", 5)
        .style("fill", "orange")
        .on('click', function(data) {
          $('.polaroid').fadeOut(function() {
            $('#info').delay(400).html('<h2 class="lobster">' + data.place + ' ' + data.date + '</h2><p class="center">' + data.text + '</p>');
            $('#picture').delay(400).html('<a href="img/' + year + '/' + data.url + '" target="_blank"><img src="img/' + year + '/' + data.url + '"></img></a>');
            $('.polaroid').fadeIn();
          });
        });
    });
  };

  // load and display the World
  d3.json("js/world.json", function(error, topology) {
    g.selectAll("path")
      .data(topojson.object(topology, topology.objects.countries)
        .geometries)
      .enter()
      .append("path")
      .attr("d", path);
      updatePoints(currentYear);
  });

  // zoom and pan
  var zoom = d3.behavior.zoom()
    .on("zoom", function() {
      g.attr("transform", "translate(" +
        d3.event.translate.join(",") + ")scale(" + d3.event.scale + ")");
      g.selectAll("circle")
        .attr("d", path.projection(projection))
        .attr("r", 5 / d3.event.scale)
      g.selectAll("path")
        .attr("d", path.projection(projection));
    });

  svg.call(zoom);

  var currentYear = new Date().getFullYear();
  $(".year").click(function(e) {
    var element = $(e.target);
    updatePoints(element.text());
  });
});
