$(document).ready(function() {
  $(".flipcard").not(".noflip").on("click", function(e) {
      $(e.target).closest(".flipcard").toggleClass("flipped");
  });
});
